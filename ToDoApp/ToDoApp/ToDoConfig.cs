﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace ToDoApp
{
    class ToDoConfig
    {
        private const string filename = "config.dat";
        private static BinaryFormatter formatter = new BinaryFormatter();
        public static Boolean case_sensitivity = false;
        public static Boolean soft_search = true;
        public static Boolean view_Checked = true;

        public static void save()
        {
            FileStream stream = File.Create(filename);
            formatter.Serialize(stream, case_sensitivity);
            formatter.Serialize(stream, soft_search);
            formatter.Serialize(stream, view_Checked);
            stream.Close();
        }

        public static void load()
        {
            if (File.Exists(filename))
            {
                FileStream stream = File.OpenRead(filename);
                case_sensitivity = (Boolean)formatter.Deserialize(stream);
                soft_search = (Boolean)formatter.Deserialize(stream);
                view_Checked = (Boolean)formatter.Deserialize(stream);
                stream.Close();
            }
        }
    }
}
