﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ToDoApp
{
    public partial class ToDoOptions_F : Form
    {

        public ToDoOptions_F()
        {
            InitializeComponent();
            loadFromConfig();
        }

        private void loadFromConfig()
        {
            this.checkBox1.Checked = ToDoConfig.case_sensitivity;
            this.checkBox2.Checked = ToDoConfig.soft_search;
            this.checkBox3.Checked = ToDoConfig.view_Checked;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ToDoConfig.case_sensitivity = this.checkBox1.Checked;
            ToDoConfig.soft_search = this.checkBox2.Checked;
            ToDoConfig.view_Checked = this.checkBox3.Checked;
            ToDoConfig.save();
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

    }
}
