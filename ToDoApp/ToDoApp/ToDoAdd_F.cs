﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ToDoApp
{
    public partial class ToDoAdd_F : Form
    {
        private ToDo newToDo;

        public ToDoAdd_F()
        {
            InitializeComponent();
            this.ActiveControl = this.textBox1;
            this.BackColor = Color.LightGray;
        }

        public ToDo getnewToDo()
        {
            return this.newToDo;
        }

        public void loadToDo(ToDo tD)
        {
            this.textBox1.Text = tD.GetNamn();
            this.numericUpDown1.Value = tD.GetPrioritet();
            this.dateTimePicker1.Value = tD.GetDatum();
            this.dateTimePicker2.Value = tD.GetDatum();
            this.newToDo = tD;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Create ToDo
            if ((textBox1.Text != "") && (numericUpDown1.Value != null) && (numericUpDown1.Value > 0))
            {
                //
                int y = dateTimePicker1.Value.Year;
                int m = dateTimePicker1.Value.Month;
                int d = dateTimePicker1.Value.Day;
                int h = dateTimePicker2.Value.Hour;
                int min = dateTimePicker2.Value.Minute;
                DateTime dt = new DateTime(y, m, d, h, min, 0);

                if (this.newToDo != null)
                {
                    this.newToDo = new ToDo(textBox1.Text, dt, Convert.ToInt32(numericUpDown1.Value), false, this.newToDo.getID());
                }
                else {
                    this.newToDo = new ToDo(textBox1.Text, dt, Convert.ToInt32(numericUpDown1.Value), false, null);
                }
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            //Remove ToDo
            this.DialogResult = System.Windows.Forms.DialogResult.No;
        }

    }
}
