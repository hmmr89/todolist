﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.SqlClient;
using System.Data.SqlServerCe;

namespace ToDoApp
{
    class ToDoDB
    {
        SqlCeConnection connection = new SqlCeConnection("Data Source = Database1.sdf");


        public List<ToDo> LoadToDoList()
        {

            string queryString = "SELECT * FROM ToDoDB";

            SqlCeCommand command = new SqlCeCommand(queryString, connection);
            connection.Open();

            SqlCeDataReader reader = command.ExecuteReader();

            List<ToDo> toDoList = new List<ToDo>();

            while (reader.Read())
            {
                toDoList.Add(new ToDo(reader.GetString(0), reader.GetDateTime(1), reader.GetInt32(2), reader.GetBoolean(3), reader.GetString(4)));
            }

            reader.Close();
            connection.Close();
            return toDoList;

        }

        public void editToDo(ToDo tD)
        {
            connection.Open();
            using (SqlCeCommand com = new SqlCeCommand("UPDATE ToDoDB SET [Namn] = @Namn, [Datum] = @Datum, [Prioritet] = @Prioritet, [Status] = @Status WHERE [UniqueID] = '" + tD.getID() + "';", connection))
            {
                com.Parameters.AddWithValue("@Namn", tD.GetNamn());
                com.Parameters.AddWithValue("@Datum", tD.GetDatum());
                com.Parameters.AddWithValue("@Prioritet", tD.GetPrioritet());
                com.Parameters.AddWithValue("@Status", tD.getStatus());
                com.ExecuteNonQuery();
            }

            connection.Close();
        }

        public void saveToDo(ToDo tD)
        {
            connection.Open();

            using (SqlCeCommand com = new SqlCeCommand("INSERT INTO ToDoDB (Namn, Datum, Prioritet, Status, UniqueID) Values(@Namn, @Datum, @Prioritet, @Status, @UniqueID)", connection))
            {
                com.Parameters.AddWithValue("@Namn", tD.GetNamn());
                com.Parameters.AddWithValue("@Datum", tD.GetDatum());
                com.Parameters.AddWithValue("@Prioritet", tD.GetPrioritet());
                com.Parameters.AddWithValue("@Status", tD.getStatus());
                com.Parameters.AddWithValue("@UniqueID", tD.getID());

                com.ExecuteNonQuery();
            }

            connection.Close();

        }

        public void deleteToDo(ToDo tD)
        {
            connection.Open();

            using (SqlCeCommand com = new SqlCeCommand("DELETE FROM ToDoDB WHERE [UniqueID] = '" + tD.getID() + "';", connection))
            {
                com.ExecuteNonQuery();
            }
            connection.Close();
        }

    }
}
