﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace ToDoApp
{

    public partial class ToDoApplication_F : Form
    {
        private static List<ToDo> toDoList = new List<ToDo>();
        private static ToDoDB db = new ToDoDB();

        public ToDoApplication_F()
        {
            InitializeComponent();
            updateFromDB();
            ToDoConfig.load();
            this.BackColor = Color.LightGray;
        }

        private void updateFromDB()
        {
            toDoList = db.LoadToDoList();
            ToDoConfig.load();
            updateTable();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Skapar ToDo
            ToDoAdd_F tdA = new ToDoAdd_F();
            tdA.Text = "Add";
            DialogResult result = tdA.ShowDialog();
            if (result == DialogResult.OK)
            {
                toDoList.Add(tdA.getnewToDo());
                db.saveToDo(tdA.getnewToDo());
                updateTable();
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.ColumnIndex > 0) && (e.RowIndex >= 0))
            {
                //EditFrame
                ToDoAdd_F tdA = new ToDoAdd_F();
                tdA.Text = "Edit";
                string id = (string)dataGridView1.Rows[e.RowIndex].Cells[4].Value;
                ToDo tD = searchToDoList(id);
                if (tD != null)
                {
                    tdA.loadToDo(tD);
                    DialogResult result = tdA.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        //Renew
                        toDoList.Remove(tD);
                        toDoList.Add(tdA.getnewToDo());
                        db.editToDo(tdA.getnewToDo());
                        updateTable();
                    }
                }
            }
        }

        private ToDo searchToDoList(string uniqueID)
        {
            foreach (ToDo tD in toDoList)
            {
                if (tD.getID() == uniqueID)
                {
                    return tD;
                }
            }
            return null;
        }


        private void updateTable()
        {
            dataGridView1.Rows.Clear();

            int index = 0;
            foreach (ToDo tD in toDoList)
            {
                object[] values = new object[5];
                values[0] = false;
                values[1] = tD.GetNamn();
                values[2] = tD.GetDatumStringFormat();
                values[3] = tD.GetPrioritet();
                values[4] = tD.getID();
                dataGridView1.Rows.Insert(index, values);

                if (tD.getStatus() == true)
                {
                    dataGridView1.Rows[index].DefaultCellStyle.BackColor = Color.LightGreen;
                }
                else
                {
                    if (DateTime.Now > tD.GetDatum())
                    {
                        dataGridView1.Rows[index].DefaultCellStyle.BackColor = Color.Red;
                    }
                }
            }
            search();

            if ((dataGridView1.SortOrder != null) && (dataGridView1.SortedColumn != null))
            {
                if (dataGridView1.SortOrder.ToString() == "Ascending")
                {
                    dataGridView1.Sort(dataGridView1.SortedColumn, ListSortDirection.Ascending);
                }
                else
                {
                    dataGridView1.Sort(dataGridView1.SortedColumn, ListSortDirection.Descending);
                }
            }
            dataGridView1.ClearSelection();
        }

        public void updateVisible()
        {
            if (ToDoConfig.view_Checked == false)
            {
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    string id = (string)row.Cells[4].Value;
                    ToDo tD = searchToDoList(id);

                    if (tD.getStatus() == true)
                    {
                        row.Visible = false;
                    }
                }

            }
        }


        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            search();
        }

        private void search()
        {
            //Sök metoden.
            int prioritet = 0;
            string id;
            Match result;
            ToDo tD;

            foreach (DataGridViewRow row in dataGridView1.Rows)
            {

                id = (string)row.Cells[4].Value;
                tD = searchToDoList(id);
                string search = textBox1.Text;


                //Filtrera prioritet
                result = Regex.Match(textBox1.Text, "(#" + "\\d{1,3}).*");
                if (result.Success)
                {
                    search = Regex.Split(textBox1.Text, "\\s")[0];
                    try
                    {
                        prioritet = Convert.ToInt32(Regex.Replace(search, "#", ""));
                    }
                    catch (FormatException)
                    {
                        prioritet = 0;
                    }
                    search = Regex.Replace(textBox1.Text, "#\\d{1,3}\\s{0,1}", "");
                }


                if ((prioritet == 0) || (tD.GetPrioritet() == prioritet))
                {

                    //Sök Namn&Datum
                    if (!ToDoConfig.case_sensitivity)
                    {
                        result = Regex.Match(tD.GetNamn().ToLower(), ".*" + search.ToLower() + ".*");
                    }
                    else
                    {
                        result = Regex.Match(tD.GetNamn(), ".*" + search + ".*");
                    }
                    if (result.Success)
                    {
                        row.Visible = true;
                    }
                    else
                    {
                        result = Regex.Match(tD.GetDatum().ToString(), ".*" + search + ".*");

                        if (result.Success)
                        {
                            row.Visible = true;
                        }
                        else
                        {
                            row.Visible = false;
                        }

                    }
                }
            }
            updateVisible();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if ((Boolean)row.Cells[0].Value == true)
                {
                    string id = (string)row.Cells[4].Value;
                    ToDo tD = searchToDoList(id);
                    tD.setStatus(true);
                    db.editToDo(tD);
                    row.Cells[0].Value = false;
                }
            }
            updateTable();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if ((Boolean)row.Cells[0].Value == true)
                {
                    string id = (string)row.Cells[4].Value;
                    ToDo tD = searchToDoList(id);
                    db.deleteToDo(tD);
                    toDoList.Remove(tD);
                    row.Cells[0].Value = false;
                }
            }
            updateTable();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //Skapar ToDo
            ToDoOptions_F tdO = new ToDoOptions_F();
            DialogResult result = tdO.ShowDialog();
            updateTable();
        }
    }
}
