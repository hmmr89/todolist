﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToDoApp
{
    public class ToDo
    {
        private string namn;
        private DateTime datum;
        private int prioritet;
        private Boolean status;
        private string uniqueID;

        public ToDo(string namn, DateTime datum, int prioritet, Boolean status, String uniqueID)
        {
            this.namn = namn;
            this.datum = datum;
            this.prioritet = prioritet;
            this.status = status;
            if (uniqueID == null)
            {
                generateID();
            }
            else
            {
                this.uniqueID = uniqueID;
            }
        }

        public string GetNamn()
        {
            return this.namn;
        }
        public DateTime GetDatum()
        {
            return this.datum;
        }
        public String GetDatumStringFormat()
        {
            String date = this.datum.ToShortDateString();
            String time = this.datum.ToShortTimeString();
            return (date + " " + time);
        }

        public int GetPrioritet()
        {
            return this.prioritet;
        }

        private void generateID()
        {
            Guid guid = Guid.NewGuid();
            uniqueID = guid.ToString();
        }

        public string getID()
        {
            return this.uniqueID;
        }

        public Boolean getStatus()
        {
            return this.status;
        }

        public void setStatus(Boolean status)
        {
            this.status = status;
        }
    }
}
